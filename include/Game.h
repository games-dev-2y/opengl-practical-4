#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
/// <summary>
/// This will hookup OpenGL
/// to our game
/// </summary>
#include <gl/GL.h>
#include <gl/GLU.h>

using namespace std;
using namespace sf;

class Game
{
public:
	Game(ContextSettings settings = ContextSettings());
	~Game();
	void run();
private:
	void initialize();
	void update();
	void draw();
	void unload();

	Window m_window;
	bool m_isRunning = false;
	const int m_primatives;

	GLuint m_index;
	Clock m_clock;
	Time m_elapsed;

	float m_rotationAngle;
};