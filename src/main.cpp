/// <summary>
/// @mainpage OpenGL Practical 4
/// @Author Rafael Girao
/// @Version 1.0
///
/// Time Spent:
///		09th/11/2016 21:00 30min
///
/// Total Time Taken:
///		0hr 30min
/// </summary>


#include "Game.h"


int main(void)
{
	ContextSettings settings;
	settings.depthBits = 24u;
	settings.stencilBits = 8u;
	settings.antialiasingLevel = 8u;
	settings.majorVersion = 3u;
	settings.minorVersion = 0u;

	Game& game = Game(settings);
	game.run();
}