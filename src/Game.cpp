#include "Game.h"

bool flip = false;
int current = 1;

Game::Game(ContextSettings settings) :
	m_window(VideoMode(800, 600), "OpenGL", Style::Default, settings),
	m_primatives(2),
	m_rotationAngle(0.0f)
{
	m_index = glGenLists(m_primatives);
}

Game::~Game() {}

void Game::run()
{

	initialize();

	Event event;

	while (m_isRunning) {

		cout << "Game running..." << endl;

		while (m_window.pollEvent(event))
		{
			if (event.type == Event::Closed)
			{
				m_isRunning = false;
			}
		}
		update();
		draw();
	}

}

void Game::initialize()
{
	m_isRunning = true;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, m_window.getSize().x / m_window.getSize().y, 1.0, 500.0);
	glMatrixMode(GL_MODELVIEW);


	glNewList(m_index, GL_COMPILE);
	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.0, 2.0, -5.0);
		glVertex3f(-2.0, -2.0, -5.0);
		glVertex3f(2.0, -2.0, -5.0);
	}
	glEnd();
	glEndList();

	glNewList(m_index + 1, GL_COMPILE);
	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, -2.0f);
		glVertex3f(-2.0f, -2.0f, -2.0f);
		glVertex3f(2.0f, -2.0f, -2.0f);
	}
	glEnd();
	glEndList();
}

void Game::update()
{
	m_elapsed = m_clock.getElapsedTime();

	if (m_elapsed.asSeconds() >= 1.0f)
	{
		m_clock.restart();

		if (!flip)
		{
			flip = true;
			current++;
			if (current > m_primatives)
			{
				current = 1;
			}
		}
		else
			flip = false;
	}

	if (flip)
	{
		m_rotationAngle += 0.05f;

		if (m_rotationAngle > 360.0f)
		{
			m_rotationAngle -= 360.0f;
		}
	}

	cout << "Update up" << endl;
}

void Game::draw()
{
	cout << "Draw up" << endl;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Investigate Here!!!!!
	glLoadIdentity(); // Resets camera transformations (required for proper rotations)
	glRotatef(m_rotationAngle, 0.0f, 0.0f, 1.0f); // Transforms camera by rotating it around the z-axis
	glTranslatef(0.0f, 0.0f, -4.0f); // Transforms camera by moving it a set amount
	glScalef(1.5f, 1.0f, 1.0f); // Distorts camera perspective by transforming the camera scale to be 50% wider)

	cout << "Drawing Primative " << current << endl;
	glCallList(current);

	m_window.display();

}

void Game::unload()
{
	cout << "Cleaning up" << endl;
}

